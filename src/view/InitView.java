package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class InitView extends JFrame {

	private JPanel contentPane;
	private JTextField txtTamanhoInterno1;
	private JTextField txtTamanhoInterno2;
	private JLabel lblTamanhosExternos;
	private JTextField txtTamanhoExterno1;
	private JLabel lblXe;
	private JTextField txtTamanhoExterno2;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InitView frame = new InitView();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public InitView() {
		setTitle("Or\u00E7amento - S\u00F3 Posters");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		txtTamanhoInterno1 = new JTextField();
		txtTamanhoInterno1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		txtTamanhoInterno1.setBounds(10, 36, 86, 30);
		contentPane.add(txtTamanhoInterno1);
		txtTamanhoInterno1.setColumns(10);

		txtTamanhoInterno2 = new JTextField();
		txtTamanhoInterno2.setBounds(125, 36, 86, 30);
		contentPane.add(txtTamanhoInterno2);
		txtTamanhoInterno2.setColumns(10);

		JLabel lblTamanhosInternos = new JLabel("Tamanhos internos");
		lblTamanhosInternos.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblTamanhosInternos.setBounds(10, 11, 136, 14);
		contentPane.add(lblTamanhosInternos);

		JLabel lblXi = new JLabel("X");
		lblXi.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblXi.setBounds(106, 37, 9, 14);
		contentPane.add(lblXi);

		lblTamanhosExternos = new JLabel("Tamanhos externos");
		lblTamanhosExternos.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblTamanhosExternos.setBounds(10, 67, 139, 14);
		contentPane.add(lblTamanhosExternos);

		txtTamanhoExterno1 = new JTextField();
		txtTamanhoExterno1.setBounds(10, 92, 86, 30);
		contentPane.add(txtTamanhoExterno1);
		txtTamanhoExterno1.setColumns(10);

		lblXe = new JLabel("X");
		lblXe.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblXe.setBounds(106, 93, 9, 14);
		contentPane.add(lblXe);

		txtTamanhoExterno2 = new JTextField();
		txtTamanhoExterno2.setBounds(125, 92, 86, 30);
		contentPane.add(txtTamanhoExterno2);
		txtTamanhoExterno2.setColumns(10);

		JButton btnClonar = new JButton("Clonar");
		btnClonar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (txtTamanhoInterno1.getText().isEmpty() || txtTamanhoInterno2.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Os tamanhos internos precisam estar preenchidos corretamente");
				} else {
					String copiaInterno1 = txtTamanhoInterno1.getText();
		            String copiaInterno2 = txtTamanhoInterno2.getText();
		            txtTamanhoExterno1.setText(copiaInterno1);
		            txtTamanhoExterno2.setText(copiaInterno2);
				}

			}
		});
		btnClonar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnClonar.setBounds(221, 91, 89, 23);
		contentPane.add(btnClonar);
	}
}
